package org.maniscalco.android.trainernotebook;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.UUID;

public class RepetitionListActivity extends DualFragmentActivity {

    private static final String EXTRA_TRAINING_UUID = "org.maniscalco.android.trainernotebook.training.uuid";

    public static Intent newIntent(Context packageContext, UUID trainingId) {
        Intent intent = new Intent(packageContext, RepetitionListActivity.class);
        intent.putExtra(EXTRA_TRAINING_UUID, trainingId);
        return intent;
    }

    @Override
    protected android.support.v4.app.Fragment createUpFragment() {
        UUID traId = (UUID) getIntent().getSerializableExtra(EXTRA_TRAINING_UUID);
        return TrainingFragment.newInstance(traId);
    }

    @Override
    protected android.support.v4.app.Fragment createDownFragment() {
        UUID traId = (UUID) getIntent().getSerializableExtra(EXTRA_TRAINING_UUID);
        return RepetitionListFragment.newInstance(traId);
    }
}
