package org.maniscalco.android.trainernotebook;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.UUID;

public class TrainingListActivity extends DualFragmentActivity {

    private static final String EXTRA_PLAN_UUID = "org.maniscalco.android.trainernotebook.plan.uuid";

    public static Intent newIntent(Context packageContext, UUID planId) {
        Intent intent = new Intent(packageContext, TrainingListActivity.class);
        intent.putExtra(EXTRA_PLAN_UUID, planId);
        return intent;
    }

    @Override
    protected Fragment createUpFragment() {
        UUID planId = (UUID) getIntent().getSerializableExtra(EXTRA_PLAN_UUID);
        return PlanFragment.newInstance(planId);
    }

    @Override
    protected Fragment createDownFragment() {
        UUID planId = (UUID) getIntent().getSerializableExtra(EXTRA_PLAN_UUID);
        return TrainingListFragment.newInstance(planId);
    }
}
