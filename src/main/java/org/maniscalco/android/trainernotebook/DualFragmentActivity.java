package org.maniscalco.android.trainernotebook;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public abstract class DualFragmentActivity extends AppCompatActivity {

    protected abstract Fragment createUpFragment();
    protected abstract Fragment createDownFragment();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dual_fragment);
        FragmentManager fm = getSupportFragmentManager();
        Fragment upFragment = fm.findFragmentById(R.id.up_fragment_container);
        Fragment downFragment = fm.findFragmentById(R.id.down_fragment_container);
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        if (upFragment == null) {
            upFragment = createUpFragment();
            fragmentTransaction.add(R.id.up_fragment_container, upFragment);
        }

        if (downFragment == null) {
            downFragment = createDownFragment();
            fragmentTransaction.add(R.id.down_fragment_container, downFragment);
        }

        fragmentTransaction.commit();
    }
}
