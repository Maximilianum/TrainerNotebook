package org.maniscalco.android.trainernotebook;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;
import java.util.UUID;

public class ExerciseTypeSelectionDialogFragment extends DialogFragment {

    private static final String ARG_EXERCISE_TYPE_UUID = "org.maniscalco.android.trainernotebook.exercise.type.id";
    public static final String EXTRA_EXERCISE_TYPE_UUID = "org.maniscalco.android.trainernotebook.exercise.type.id";

    private ExerciseType mExerciseType;
    private RecyclerView mRecyclerView;
    private ExerciseAdapter mAdapter;
    private TextView mEmptyView;

    public static ExerciseTypeSelectionDialogFragment newInstance(UUID exeId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_EXERCISE_TYPE_UUID, exeId);
        ExerciseTypeSelectionDialogFragment fragment = new ExerciseTypeSelectionDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        try {
            UUID typeId = (UUID) getArguments().getSerializable(ARG_EXERCISE_TYPE_UUID);
            mExerciseType = TrainerLab.get(getActivity()).getExerciseType(typeId);
        } catch (NullPointerException e) {
            System.out.println(e);
        }
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_exercise_type_list, null);

        mRecyclerView = view.findViewById(R.id.exercise_type_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mEmptyView = view.findViewById(R.id.empty_exercise_types_list_message);
        updateUI();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setTitle(R.string.exercise_type_selection_dialog);
        return builder.create();
    }

    /*@Override
    public void onPause() {
        super.onPause();
    }*/

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    /*@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SAVED_SUBTITLE_VISIBLE, mSubtitleVisible);
    }*/

    private void sendResult(int resultCode, UUID typeId) {
        if (getTargetFragment() != null) {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_EXERCISE_TYPE_UUID, typeId);
            getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
        }
    }

    private void updateUI() {
        TrainerLab trainerLab = TrainerLab.get(getActivity());
        List<ExerciseType> exerciseTypes = trainerLab.getExerciseTypes(null, null);

        if (exerciseTypes.size() > 0) {
            mEmptyView.setVisibility(View.GONE);
        } else {
            mEmptyView.setVisibility(View.VISIBLE);
        }

        if (mAdapter == null) {
            mAdapter = new ExerciseAdapter(exerciseTypes, mExerciseType.getId());
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setExercises(exerciseTypes, mExerciseType.getId());
            mAdapter.notifyDataSetChanged();
        }
    }

    private class ExerciseHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ExerciseType mExerciseType;
        private UUID mSelectedType;
        private CheckBox mCheckBox;
        private TextView mTitleView;
        private TextView mTargetView;

        public ExerciseHolder(final View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mCheckBox = itemView.findViewById(R.id.list_item_exercise_checkbox);
            mTitleView = itemView.findViewById(R.id.list_item_exercise_title_view);
            mTargetView = itemView.findViewById(R.id.list_item_exercise_target_view);
        }

        public void setSelectedType(UUID typeId) {
            mSelectedType = typeId;
        }

        public void bindExercise(ExerciseType exe) {

            mExerciseType = exe;
            /*
             * onCheckedChangeListener() would be triggered when setting the state with setChecked()
             * so I disable the listener by passing null and re-enable soon after setting its state
             */
            mCheckBox.setOnCheckedChangeListener(null);
            mCheckBox.setChecked(exe.getId().equals(mSelectedType));
            mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    onClick(mCheckBox);
                }
            });
            mTitleView.setText(exe.getTitle());
            mTargetView.setText(exe.getTarget());
        }

        @Override
        public void onClick(View v) {
            sendResult(Activity.RESULT_OK, mExerciseType.getId());
            dismiss();
        }
    }

    private class ExerciseAdapter extends RecyclerView.Adapter<ExerciseHolder> {

        private List<ExerciseType> mExerciseTypes;
        UUID mSelectedType;

        public ExerciseAdapter(List<ExerciseType> exerciseTypes, UUID selected) {
            mExerciseTypes = exerciseTypes;
            mSelectedType = selected;
        }

        @Override
        public ExerciseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

            View view = layoutInflater.inflate(R.layout.item_list_exercise_type_checkable, parent, false);
            ExerciseHolder exerciseHolder = new ExerciseHolder(view);
            exerciseHolder.setSelectedType(mSelectedType);
            return exerciseHolder;
        }

        @Override
        public void onBindViewHolder(ExerciseHolder holder, int position) {

            ExerciseType exe = mExerciseTypes.get(position);
            holder.setSelectedType(mSelectedType);
            holder.bindExercise(exe);
        }

        @Override
        public int getItemCount() {
            return mExerciseTypes.size();
        }

        public void setExercises(List<ExerciseType> exerciseTypes, UUID selected) {
            mExerciseTypes = exerciseTypes;
            mSelectedType = selected;
        }
    }
}
