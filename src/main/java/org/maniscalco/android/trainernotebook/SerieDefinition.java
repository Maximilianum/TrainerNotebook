package org.maniscalco.android.trainernotebook;

import android.util.Log;

import java.util.Locale;

public class SerieDefinition {

    private static final String FIXED = "fixed";
    private static final String PYRAMID = "pyramid";
    private static final String RHOMBUS = "rhombus";

    public enum Type { fixed, pyramid, rhombus };

    private Type type;
    private int serie;
    private int superSerie;
    private int repetition;
    private int serieMin;
    private int serieStep;
    private double load;
    private double loadMin;
    private double loadStep;

    public SerieDefinition() {
        type = Type.fixed;
        serie = 10;
        repetition = 3;
    }

    public Type getType() {
        return type;
    }

    public String getTypeString() {
        switch (type) {
            case fixed:
                return FIXED;
            case pyramid:
                return PYRAMID;
            case rhombus:
        }
        return RHOMBUS;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setType(String typeString) {
        switch (typeString) {
            case SerieDefinition.FIXED:
                this.type = Type.fixed;
                break;
            case SerieDefinition.PYRAMID:
                this.type = Type.pyramid;
                break;
            default:
                this.type = Type.rhombus;
                break;
        }
    }

    public int getSerie() {
        return serie;
    }

    public void setSerie(int serie) {
        this.serie = serie;
    }

    public int getSuperSerie() {
        return superSerie;
    }

    public void setSuperSerie(int super_serie) {
        this.superSerie = super_serie;
    }

    public int getRepetition() {
        return repetition;
    }

    public void setRepetition(int repetition) {
        this.repetition = repetition;
    }

    public int getSerieMin() {
        return serieMin;
    }

    public void setSerieMin(int serie_min) {
        this.serieMin = serie_min;
    }

    public int getSerieStep() {
        return serieStep;
    }

    public void setSerieStep(int serie_step) {
        this.serieStep = serie_step;
    }

    public double getLoad() {
        return load;
    }

    public void setLoad(double load) {
        this.load = load;
    }

    public double getLoadMin() {
        return loadMin;
    }

    public void setLoadMin(double load_min) {
        this.loadMin = load_min;
    }

    public double getLoadStep() {
        return loadStep;
    }

    public void setLoadStep(double loadStep) {
        this.loadStep = loadStep;
    }

    private double calculateLoadStep() {
        return (load - loadMin) / ((serie - serieMin) / serieStep);
    }

    public String getSeriesString() {
        String series = "";

        switch (type) {
            case fixed:
                if (superSerie > 0) {
                    series = String.valueOf(repetition) + " x (" + String.valueOf(superSerie) + " x " + String.valueOf(serie) + ")";
                } else {
                    series = String.valueOf(repetition) + " x " + String.valueOf(serie);
                }
                break;
            case pyramid:
                if (serieStep > 0) {
                    for (int i = serie; i > serieMin; i -= serieStep) {
                        series += String.valueOf(i) + ", ";
                    }
                    series += String.valueOf(serieMin);
                }
                break;
            case rhombus:
                if (serieStep > 0) {
                    for (int i = serie; i > serieMin; i -= serieStep) {
                        series += String.valueOf(i) + ", ";
                    }
                    series += String.valueOf(serieMin) + ", ";
                    for (int i = serieMin + serieStep; i < serie; i += serieStep) {
                        series += String.valueOf(i) + ", ";
                    }
                    series += String.valueOf(serie);
                }
                break;
        }

        return series;
    }

    public int getSeriesSize() {
        int n = 0;
        switch (type) {
            case fixed:
                return repetition;
            case pyramid:
                if (serieStep > 0) {
                    n = ((serie - serieMin) / serieStep) + 1;
                }
                break;
            case rhombus:
                if (serieStep > 0) {
                    n = (((serie - serieMin) / serieStep) * 2) + 1;
                }
                break;
        }
        return n;
    }

    public String getLoadsString() {
        String loads = "";

        switch (type) {
            case fixed:
                if (superSerie > 0) {
                    loads = String.format(Locale.getDefault(), "(%1$,.2f, %2$,.2f)", load, loadMin);
                } else {
                    loads = String.format(Locale.getDefault(), "%1$,.2f", load);
                }
                break;
            case pyramid:
            case rhombus:
                if (serieStep > 0) {
                    double lstep = calculateLoadStep();
                    int n = Math.toIntExact(Math.round((load - loadMin) / lstep));
                    if (loadStep > 0.0d) {
                        loads = String.format(Locale.getDefault(), "%1$,.2f, ", loadMin);
                        double roundedVal = loadMin;
                        double realVal = loadMin;
                        for (int i = 1; i < n; i++) {
                            realVal += lstep;
                            roundedVal += loadStep;
                            while ((realVal - roundedVal) > (loadStep / 2.0d)) {
                                roundedVal += loadStep;
                            }
                            loads += String.format(Locale.getDefault(), "%1$,.2f, ", roundedVal);
                        }
                        loads += String.format(Locale.getDefault(), "%1$,.2f", load);
                    }
                }
                break;
        }

        return loads;
    }

    public String getSerieAt(int index) {

        String serieString = "";
        int n;
        int val;

        switch (type) {
            case fixed:
                if (superSerie > 0) {
                    serieString = String.valueOf(superSerie) + " x " + String.valueOf(serie);
                } else {
                    serieString = String.valueOf(serie);
                }
                break;
            case pyramid:
                // calculate the value of the serie at given index
                val = serie - (serieStep * index);
                if (val >= serieMin) {
                    serieString = String.valueOf(val);
                } else {
                    serieString = String.valueOf(serieMin);
                }
                break;
            case rhombus:
                // calculate the number of steps to arrive at serieMin
                if (serieStep > 0) {
                    n = (serie - serieMin) / serieStep;
                    if (index < n + 1) {
                        val = serie - (serieStep * index);
                        if (val >= serieMin) {
                            serieString = String.valueOf(val);
                        } else {
                            serieString = String.valueOf(serieMin);
                        }
                    } else if (index > n) {
                        val = serieMin + (serieStep * (index - n));
                        if (val < serie) {
                            serieString = String.valueOf(val);
                        } else {
                            serieString = String.valueOf(serie);
                        }
                    }
                }
                break;
        }
        return serieString;
    }

    public String getLoadAt(int index) {

        String loadString = "";

        switch (type) {
            case fixed:
                if (superSerie > 0) {
                    loadString = String.format(Locale.getDefault(), "(%1$,.2f, %2$,.2f)", load, loadMin);
                } else {
                    loadString = String.format(Locale.getDefault(), "%1$,.2f", load);
                }
                break;
            case pyramid:
                if (serieStep > 0) {
                    double lstep = calculateLoadStep();
                    int n = Math.toIntExact(Math.round((load - loadMin) / lstep));
                    if (index <= n && loadStep > 0.0d) {
                        double roundedVal = loadMin;
                        double realVal = loadMin;
                        for (int i = 0; i < index; i++) {
                            realVal += lstep;
                            roundedVal += loadStep;
                            while ((realVal - roundedVal) > (loadStep / 2.0d)) {
                                roundedVal += loadStep;
                            }
                        }
                        loadString = String.format(Locale.getDefault(), "%1$,.2f", roundedVal);
                    }
                }
                break;
            case rhombus:
                if (serieStep > 0) {
                    double lstep = calculateLoadStep();
                    int n = Math.toIntExact(Math.round((load - loadMin) / lstep));
                    if (loadStep > 0.0d) {
                        if (index <= n) {
                            double roundedVal = loadMin;
                            double realVal = loadMin;
                            for (int i = 0; i < index; i++) {
                                realVal += lstep;
                                roundedVal += loadStep;
                                while ((realVal - roundedVal) > (loadStep / 2.0d)) {
                                    roundedVal += loadStep;
                                }
                            }
                            loadString = String.format(Locale.getDefault(), "%1$,.2f", roundedVal);
                        } else if (index > n) {
                            double roundedVal = load;
                            double realVal = load;
                            for (int i = n; i < index; i++) {
                                realVal -= lstep;
                                roundedVal -= loadStep;
                                while ((roundedVal - realVal) > (loadStep / 2.0d)) {
                                    roundedVal -= loadStep;
                                }
                            }
                            loadString = String.format(Locale.getDefault(), "%1$,.2f", roundedVal);
                        }
                    }
                }
                break;
        }
        return loadString;
    }

    public boolean indexIsValid(int index) {
        boolean valid = false;

        switch (type) {
            case fixed:
                valid = index < repetition;
                break;
            case pyramid:
                if (serieStep > 0) {
                    valid = index <= ((serie - serieMin) / serieStep);
                }
                break;
            case rhombus:
                if (serieStep > 0) {
                    valid = index <= (((serie - serieMin) / serieStep) * 2);
                }
                break;
        }
        return valid;
    }
}

