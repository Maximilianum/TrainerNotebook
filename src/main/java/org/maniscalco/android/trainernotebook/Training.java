package org.maniscalco.android.trainernotebook;

import java.util.UUID;

public class Training {
    private UUID mId;
    private UUID mPlan;
    private int mNumber;

    public Training () {
        // Generate unique identifier
        this(UUID.randomUUID());
        mNumber = 0;
    }

    public Training(UUID id) { mId = id; }

    public UUID getId() {
        return mId;
    }

    public UUID getPlan() {
        return mPlan;
    }

    public void setPlan(UUID mPlan) {
        this.mPlan = mPlan;
    }

    public int getNumber() {
        return mNumber;
    }

    public void setNumber(int val) {
        this.mNumber = val;
    }
}
