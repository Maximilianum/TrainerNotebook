package org.maniscalco.android.trainernotebook;

public class PlanListActivity extends SingleFragmentActivity {

    @Override
    protected android.support.v4.app.Fragment createFragment() {
        return new PlanListFragment();
    }
}
