package org.maniscalco.android.trainernotebook;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.UUID;

public class PlanFragment extends Fragment {

    private static final String ARG_PLAN_UUID = "org.maniscalco.android.trainernotebook.plan.uuid";

    private Plan mPlan;
    private TextView mStartDateView;
    private EditText mDaysField;

    public static PlanFragment newInstance(UUID planId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_PLAN_UUID, planId);
        PlanFragment fragment = new PlanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            UUID planId = (UUID) getArguments().getSerializable(ARG_PLAN_UUID);
            mPlan = TrainerLab.get(getActivity()).getPlan(planId);
        } catch (NullPointerException e) {
            System.out.println(e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        TrainerLab.get(getActivity()).updatePlan(mPlan);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_plan, container, false);

        mStartDateView = view.findViewById(R.id.plan_start);
        mStartDateView.setText(DateFormat.getDateInstance().format(mPlan.getStartMillis()));

        mDaysField = view.findViewById(R.id.plan_days);
        if (mPlan.getDays() == 0) {
            mDaysField.setText("");
        } else {
            mDaysField.setText(String.valueOf(mPlan.getDays()));
        }
        mDaysField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")) {

                    if (s.toString().matches("\\d+")) {
                        int val = Integer.parseInt(s.toString());
                        if (val > 0) {
                            mPlan.setDays(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mPlan.setDays(0);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                }
            }
        });

        return view;
    }
}
