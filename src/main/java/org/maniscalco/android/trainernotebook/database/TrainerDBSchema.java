package org.maniscalco.android.trainernotebook.database;

public class TrainerDBSchema {

    public static final class ExerciseTypeTable {

        public static final String NAME = "exe_types";

        public static final class Cols {

            public static final String UUID = "type_uuid";
            public static final String TITLE = "title";
            public static final String TARGET = "target";
        }
    }

    public static final class RepetitionTable {

        public static final String NAME = "repetitions";

        public static final class Cols {

            public static final String UUID = "rep_uuid";
            public static final String TRAINING = "training_uuid";
            public static final String TYPE = "type_uuid";
            public static final String SERIE_TYPE = "serie_type";
            public static final String SERIE = "serie";
            public static final String SUPER = "super";
            public static final String REPETITION = "repetition";
            public static final String SERIE_MIN = "serie_min";
            public static final String SERIE_STEP = "serie_step";
            public static final String LOAD = "load";
            public static final String LOAD_MIN = "load_min";
            public static final String LOAD_STEP = "load_step";
            public static final String ORDER = "rep_order";
            public static final String REST = "rest";
            public static final String CARRIED_OUT = "carried_out";
            public static final String LAST_TIME = "last_time";
            public static final String NOTE = "note";
        }
    }

    public static final class TrainingTable {

        public static final String NAME = "trainings";

        public static final class Cols {

            public static final String UUID = "tra_uuid";
            public static final String PLAN = "plan_uuid";
            public static final String NUMBER = "number";
        }
    }

    public static final class PlanTable {

        public static final String NAME = "plans";

        public static final class Cols {

            public static final String UUID = "pla_uuid";
            public static final String START = "start";
            public static final String DAYS = "days";
        }
    }
}
