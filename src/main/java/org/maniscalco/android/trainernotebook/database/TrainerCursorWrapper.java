package org.maniscalco.android.trainernotebook.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import org.maniscalco.android.trainernotebook.ExerciseType;
import org.maniscalco.android.trainernotebook.Plan;
import org.maniscalco.android.trainernotebook.Repetition;
import org.maniscalco.android.trainernotebook.SerieDefinition;
import org.maniscalco.android.trainernotebook.Training;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.ExerciseTypeTable;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.RepetitionTable;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.TrainingTable;

import java.util.UUID;

public class TrainerCursorWrapper extends CursorWrapper {

    public TrainerCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public ExerciseType getExerciseType() {
        String uuidString = getString(getColumnIndex(ExerciseTypeTable.Cols.UUID));
        String title = getString(getColumnIndex(ExerciseTypeTable.Cols.TITLE));
        String target = getString(getColumnIndex(ExerciseTypeTable.Cols.TARGET));

        ExerciseType exerciseType = new ExerciseType(UUID.fromString(uuidString));
        exerciseType.setTitle(title);
        exerciseType.setTarget(target);

        return exerciseType;
    }

    public Repetition getRepetition() {
        String uuidString = getString(getColumnIndex(RepetitionTable.Cols.UUID));
        String uuidTraining = getString(getColumnIndex(RepetitionTable.Cols.TRAINING));
        String uuidType = getString(getColumnIndex(RepetitionTable.Cols.TYPE));
        int order = getInt(getColumnIndex(RepetitionTable.Cols.ORDER));
        int rest = getInt(getColumnIndex(RepetitionTable.Cols.REST));
        int carriedOut = getInt(getColumnIndex(RepetitionTable.Cols.CARRIED_OUT));
        long lastTime = getLong(getColumnIndex(RepetitionTable.Cols.LAST_TIME));
        String note = getString(getColumnIndex(RepetitionTable.Cols.NOTE));
        SerieDefinition serieDef = getSerieDefinition();
        Repetition rep = new Repetition(UUID.fromString(uuidString));
        rep.setTraining(UUID.fromString(uuidTraining));
        rep.setType(UUID.fromString(uuidType));
        rep.setSerie(serieDef);
        rep.setOrder(order);
        rep.setRest(rest);
        rep.setCarriedOut(carriedOut);
        rep.setLastTime(lastTime);
        rep.setNote(note);

        return rep;
    }

    private SerieDefinition getSerieDefinition() {
        String typeString = getString(getColumnIndex(RepetitionTable.Cols.SERIE_TYPE));
        int serie = getInt(getColumnIndex(RepetitionTable.Cols.SERIE));
        int superSerie = getInt(getColumnIndex(RepetitionTable.Cols.SUPER));
        int rep = getInt(getColumnIndex(RepetitionTable.Cols.REPETITION));
        int serieMin = getInt(getColumnIndex(RepetitionTable.Cols.SERIE_MIN));
        int step = getInt(getColumnIndex(RepetitionTable.Cols.SERIE_STEP));
        double load = getDouble(getColumnIndex(RepetitionTable.Cols.LOAD));
        double loadMin = getDouble(getColumnIndex(RepetitionTable.Cols.LOAD_MIN));
        double loadStep = getDouble(getColumnIndex(RepetitionTable.Cols.LOAD_STEP));

        SerieDefinition serieDef = new SerieDefinition();
        serieDef.setType(typeString);
        serieDef.setSerie(serie);
        switch (serieDef.getType()) {
            case fixed:
                serieDef.setSuperSerie(superSerie);
                serieDef.setRepetition(rep);
                break;
            case pyramid:
            case rhombus:
                serieDef.setSerieMin(serieMin);
                serieDef.setSerieStep(step);
                serieDef.setLoadStep(loadStep);
                break;
        }
        serieDef.setLoad(load);
        serieDef.setLoadMin(loadMin);
        return serieDef;
    }

    public Training getTraining() {
        String uuidString = getString(getColumnIndex(TrainingTable.Cols.UUID));
        String uuidPlan = getString(getColumnIndex(TrainingTable.Cols.PLAN));
        int number = getInt(getColumnIndex(TrainingTable.Cols.NUMBER));

        Training training = new Training(UUID.fromString(uuidString));
        training.setPlan(UUID.fromString(uuidPlan));
        training.setNumber(number);

        return training;
    }

    public Plan getPlan() {
        String uuidString = getString(getColumnIndex(TrainerDBSchema.PlanTable.Cols.UUID));
        Long start = getLong(getColumnIndex(TrainerDBSchema.PlanTable.Cols.START));
        int days = getInt(getColumnIndex(TrainerDBSchema.PlanTable.Cols.DAYS));

        Plan plan = new Plan(UUID.fromString(uuidString));
        plan.setStartMillis(start);
        plan.setDays(days);

        return plan;
    }
}
