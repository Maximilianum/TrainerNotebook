package org.maniscalco.android.trainernotebook.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.ExerciseTypeTable;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.RepetitionTable;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.TrainingTable;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.PlanTable;

public class TrainerBaseHelper extends SQLiteOpenHelper {

    private static final int VERSION = 3;
    private static final String DATABASE_NAME = "trainernotebook.db";

    public TrainerBaseHelper(Context context) {

        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + ExerciseTypeTable.NAME + "(" +
                ExerciseTypeTable.Cols.UUID + " VARCHAR(255) primary key, " +
                ExerciseTypeTable.Cols.TITLE + " VARCHAR(255), " +
                ExerciseTypeTable.Cols.TARGET + " VARCHAR(255))");

        db.execSQL("create table " + RepetitionTable.NAME + "(" +
                RepetitionTable.Cols.UUID + " VARCHAR(255) primary key, " +
                RepetitionTable.Cols.TRAINING + " VARCHAR(255), " +
                RepetitionTable.Cols.TYPE + " VARCHAR(255), " +
                RepetitionTable.Cols.SERIE_TYPE + " VARCHAR(128), " +
                RepetitionTable.Cols.SERIE + " SMALLINT, " +
                RepetitionTable.Cols.SUPER + " SMALLINT, " +
                RepetitionTable.Cols.REPETITION + " SMALLINT, " +
                RepetitionTable.Cols.SERIE_MIN + " SMALLINT, " +
                RepetitionTable.Cols.SERIE_STEP + " SMALLINT, " +
                RepetitionTable.Cols.LOAD + " REAL, " +
                RepetitionTable.Cols.LOAD_MIN + " REAL, " +
                RepetitionTable.Cols.LOAD_STEP + " REAL, " +
                RepetitionTable.Cols.ORDER + " SMALLINT, " +
                RepetitionTable.Cols.REST + " INTEGER, " +
                RepetitionTable.Cols.CARRIED_OUT + " SMALLINT, " +
                RepetitionTable.Cols.LAST_TIME + " LONGINT, " +
                RepetitionTable.Cols.NOTE + " VARCHAR(255), " +
                "foreign key (" + RepetitionTable.Cols.TYPE + ") references " +
                ExerciseTypeTable.NAME + "(" + ExerciseTypeTable.Cols.UUID + "))");

        db.execSQL("create table " + TrainingTable.NAME + "(" +
                TrainingTable.Cols.UUID + " VARCHAR(255) primary key, " +
                TrainingTable.Cols.PLAN + " VARCHAR(255), " +
                TrainingTable.Cols.NUMBER + " SMALLINT)");

        db.execSQL("create table " + PlanTable.NAME + "(" +
                PlanTable.Cols.UUID + " VARCHAR(255) primary key, " +
                PlanTable.Cols.START + " LONGINT, " +
                PlanTable.Cols.DAYS + " SMALLINT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
