package org.maniscalco.android.trainernotebook;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.UUID;

public class ExecuteDialogFragment extends DialogFragment {

    private static final String ARG_REPETITION_UUID = "org.maniscalco.android.trainernotebook.repetition.uuid";
    private static final String SAVED_MILLIS_LEFT = "org.maniscalco.android.trainernotebook.millisleft";
    private static final String SAVED_COUNTDOWN_STATE = "org.maniscalco.android.trainernotebook.countdownstate";
    private static final String SAVED_REPETITION_COUNTER = "org.maniscalco.android.trainernotebook.repetitioncounter";

    private Context mContext;
    private Repetition mRepetition;
    private ExerciseType mExerciseType;
    private ProgressBar mProgressBar;
    private TextView mTimeTextView;
    private TextView mTitleTextView;
    private TextView mSeriesTextView;
    private TextView mLoadTextView;
    private SoundPool mSoundPool;
    private int soundId;

    private CountDownTimer mCountDownTimer;
    private long mMillisLeft;
    private int mRepNumber;

    public static ExecuteDialogFragment newInstance(UUID repId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_REPETITION_UUID, repId);
        ExecuteDialogFragment fragment = new ExecuteDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mContext = getActivity();
        mSoundPool = new SoundPool.Builder().build();
        soundId = mSoundPool.load(mContext, R.raw.bell, 1);
        UUID repId = (UUID) getArguments().getSerializable(ARG_REPETITION_UUID);
        mRepetition = TrainerLab.get(mContext).getRepetition(repId);
        mExerciseType = TrainerLab.get(mContext).getExerciseType(mRepetition.getType());
        View view = LayoutInflater.from(mContext).inflate(R.layout.fragment_stopwatch, null);
        mTimeTextView = (TextView)view.findViewById(R.id.execute_countdown_view);
        mTimeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCountDownTimer == null) {
                    mMillisLeft = mRepetition.getRest() * 1000;
                    updateStopwatch();
                }
            }
        });

        mTitleTextView = (TextView)view.findViewById(R.id.time_exercise_title);
        mTitleTextView.setText(mExerciseType.getTitle());

        mSeriesTextView = (TextView)view.findViewById(R.id.time_exercise_series);
        mLoadTextView = (TextView)view.findViewById(R.id.time_exercise_load);

        mCountDownTimer = null;
        mMillisLeft = mRepetition.getRest() * 1000;

        mProgressBar = (ProgressBar) view.findViewById(R.id.stopwatch_progressbar);
        mProgressBar.setMax(mRepetition.getRest() * 1000);
        mProgressBar.setProgress(mRepetition.getRest() * 1000);
        mProgressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCountDownTimer == null) {
                    if (mMillisLeft == 0) {
                        mMillisLeft = mRepetition.getRest() * 1000;
                    }
                    mCountDownTimer = createCountDown(mMillisLeft);
                    mCountDownTimer.start();
                } else {
                    mCountDownTimer.cancel();
                    mCountDownTimer = null;
                }
            }
        });

        mRepNumber = 0;

        if (savedInstanceState != null) {
            mMillisLeft = savedInstanceState.getLong(SAVED_MILLIS_LEFT);
            if (savedInstanceState.getBoolean(SAVED_COUNTDOWN_STATE)) {
                mCountDownTimer = createCountDown(mMillisLeft);
            }
            mRepNumber = savedInstanceState.getInt(SAVED_REPETITION_COUNTER);
        }

        if (mRepetition.getSerie().indexIsValid(mRepNumber)) {
            mSeriesTextView.setText(mRepetition.getSerie().getSerieAt(mRepNumber));
            mLoadTextView.setText(mRepetition.getSerie().getLoadAt(mRepNumber));
        }

        updateStopwatch();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sendResult(Activity.RESULT_OK);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(SAVED_MILLIS_LEFT, mMillisLeft);
        outState.putBoolean(SAVED_COUNTDOWN_STATE, mCountDownTimer != null);
        outState.putInt(SAVED_REPETITION_COUNTER, mRepNumber);
    }

    private void sendResult(int resultCode) {
        if (getTargetFragment() != null) {
            Intent intent = new Intent();
            getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mSoundPool.release();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateStopwatch();
        if (mCountDownTimer != null) {
            mCountDownTimer.start();
        }
    }

    private void updateStopwatch() {
        if (mMillisLeft > 0) {
            int minutes = (int) (mMillisLeft / 60000);
            int seconds = (int) ((mMillisLeft % 60000) / 1000);
            int cents = (int) (((mMillisLeft % 60000) % 1000) / 10);
            mTimeTextView.setText(String.format("%1$02d:%2$02d:%3$02d", minutes, seconds, cents));
        } else {
            if (mRepetition.getSerie().indexIsValid(mRepNumber)) {
                mTimeTextView.setText(String.format(mContext.getResources().getString(R.string.go_exercise), mRepNumber + 1));
                mSeriesTextView.setText(mRepetition.getSerie().getSerieAt(mRepNumber));
                mLoadTextView.setText(mRepetition.getSerie().getLoadAt(mRepNumber));
            } else {
                mTimeTextView.setText(R.string.end_serie);
                mSeriesTextView.setText("");
                mLoadTextView.setText("");
            }
        }
        mProgressBar.setProgress((int)mMillisLeft);
    }

    private CountDownTimer createCountDown(long millisToFuture) {
        CountDownTimer countDownTimer = new CountDownTimer(millisToFuture, 10) {
            @Override
            public void onTick(long millisUntilFinished) {
                mMillisLeft = millisUntilFinished;
                updateStopwatch();
            }

            @Override
            public void onFinish() {

                mSoundPool.play(soundId, 1.0f, 1.0f, 1, 0, 1.0f);
                mMillisLeft = 0;
                mCountDownTimer = null;
                mRepNumber++;
                updateStopwatch();
            }
        };
        return countDownTimer;
    }
}

