package org.maniscalco.android.trainernotebook;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.maniscalco.android.trainernotebook.database.TrainerDBSchema;

import java.util.List;
import java.util.UUID;

public class RepetitionPagerActivity extends AppCompatActivity {

    private static final String EXTRA_REPETITION_ID = "org.maniscalco.android.trainernotebook.repetition.id";

    private ViewPager mViewPager;
    private List<Repetition> mRepetitions;

    public static Intent newIntent(Context packageContext, UUID repetitionId) {
        Intent intent = new Intent(packageContext, RepetitionPagerActivity.class);
        intent.putExtra(EXTRA_REPETITION_ID, repetitionId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repetition_pager);

        UUID repetitionId = (UUID) getIntent().getSerializableExtra(EXTRA_REPETITION_ID);

        mViewPager = findViewById(R.id.activity_repetition_pager_view_pager);

        Repetition rep = TrainerLab.get(this).getRepetition(repetitionId);
        mRepetitions = TrainerLab.get(this).getRepetitions(TrainerDBSchema.RepetitionTable.Cols.TRAINING + " = ?", new String[]{rep.getTraining().toString()});
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Repetition repetition= mRepetitions.get(position);
                return RepetitionFragment.newInstance(repetition.getId());
            }

            @Override
            public int getCount() {
                return mRepetitions.size();
            }
        });

        for (int i = 0; i < mRepetitions.size(); i++) {
            if (mRepetitions.get(i).getId().equals(repetitionId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }
}
