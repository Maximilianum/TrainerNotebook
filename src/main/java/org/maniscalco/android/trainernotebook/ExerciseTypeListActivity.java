package org.maniscalco.android.trainernotebook;

import android.support.v4.app.Fragment;

public class ExerciseTypeListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new ExerciseTypeListFragment();
    }
}
