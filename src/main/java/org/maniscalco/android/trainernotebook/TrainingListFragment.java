package org.maniscalco.android.trainernotebook;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.maniscalco.android.trainernotebook.database.TrainerDBSchema;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class TrainingListFragment extends Fragment {

    private static final String ARG_PLAN_UUID = "org.maniscalco.android.trainernotebook.plan.uuid";

    private Plan mPlan;
    private RecyclerView mRecyclerView;
    private TrainingAdapter mAdapter;
    private TextView mEmptyView;
    private Button mButton;

    public static TrainingListFragment newInstance(UUID planId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_PLAN_UUID, planId);
        TrainingListFragment fragment = new TrainingListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        try {
            UUID planId = (UUID) getArguments().getSerializable(ARG_PLAN_UUID);
            mPlan = TrainerLab.get(getActivity()).getPlan(planId);
        } catch (NullPointerException e) {
            System.out.println(e);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_training_list, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.training_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mEmptyView = (TextView)view.findViewById(R.id.empty_trainings_list_message);
        mButton = (Button)view.findViewById(R.id.add_new_training_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newTraining();
            }
        });

        if (savedInstanceState != null) {
            //mSubtitleVisible = savedInstanceState.getBoolean(SAVED_SUBTITLE_VISIBLE);
        }

        updateUI();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putBoolean(SAVED_SUBTITLE_VISIBLE, mSubtitleVisible);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_training_list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_training:
                newTraining();
                return true;
            case R.id.menu_item_delete_plan:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                TrainerLab.get(getActivity()).removePlan(mPlan);
                                getActivity().finish();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.confirm_delete_plan).setPositiveButton(R.string.yes, dialogClickListener).setNegativeButton(R.string.no, dialogClickListener).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateUI() {
        TrainerLab trainerLab = TrainerLab.get(getActivity());
        List<Training> trainings = trainerLab.getTrainings(TrainerDBSchema.TrainingTable.Cols.PLAN + " = ?", new String[]{mPlan.getId().toString()});

        if (trainings.size() > 0) {
            mEmptyView.setVisibility(View.GONE);
            mButton.setVisibility(View.GONE);
        } else {
            mEmptyView.setVisibility(View.VISIBLE);
            mButton.setVisibility(View.VISIBLE);
        }

        if (mAdapter == null) {
            mAdapter = new TrainingAdapter(trainings);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setTrainings(trainings);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void newTraining() {
        Training training = new Training();
        training.setPlan(mPlan.getId());
        TrainerLab.get(getActivity()).addTraining(training);
        Intent intent = RepetitionListActivity.newIntent(getActivity(), training.getId());
        startActivity(intent);
    }

    private class TrainingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Training mTraining;
        private TextView mTrainingNumberView;
        private TextView mTrainingExercisesView;

        public TrainingHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            mTrainingNumberView = (TextView) itemView.findViewById(R.id.item_list_training_number);
            mTrainingExercisesView = (TextView) itemView.findViewById(R.id.item_list_training_target);
        }

        public void bindTraining(Training training) {
            mTraining = training;
            mTrainingNumberView.setText(String.valueOf(mTraining.getNumber()));
            mTrainingExercisesView.setText(getTargetListString(mTraining.getId()));
        }

        private String getTargetListString(UUID trainingId) {
            List<String> targets = new ArrayList<String>();
            List<Repetition> reps = TrainerLab.get(getActivity()).getRepetitions(TrainerDBSchema.RepetitionTable.Cols.TRAINING + " = ?", new String[] { trainingId.toString() });
            Iterator<Repetition> iter = reps.iterator();
            while (iter.hasNext()) {
                ExerciseType exeType = TrainerLab.get(getActivity()).getExerciseType(iter.next().getType());
                if (exeType.getTarget() != null) {
                    if (targets.indexOf(exeType.getTarget()) < 0) {
                        targets.add(exeType.getTarget());
                    }
                }
            }
            String target = ": ";
            Iterator<String> iterTarget = targets.iterator();
            while (iterTarget.hasNext()) {
                if (target.length() > 2) {
                    target += ", ";
                }
                target += iterTarget.next();
            }
            if (target.length() == 0) {
                target = getActivity().getResources().getString(R.string.no_target);
            }
            return target;
        }

        @Override
        public void onClick(View v) {
            Intent intent = RepetitionListActivity.newIntent(getActivity(), mTraining.getId());
            startActivity(intent);
        }
    }

    private class TrainingAdapter extends RecyclerView.Adapter<TrainingHolder> {

        private List<Training> mTrainings;

        public TrainingAdapter(List<Training> trainings) {
            mTrainings = trainings;
        }

        @Override
        public TrainingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

            View view = layoutInflater.inflate(R.layout.item_list_training, parent, false);

            return new TrainingHolder(view);
        }

        @Override
        public void onBindViewHolder(TrainingHolder holder, int position) {

            Training training = mTrainings.get(position);
            holder.bindTraining(training);
        }

        @Override
        public int getItemCount() {
            return mTrainings.size();
        }

        public void setTrainings(List<Training> trainings) {
            mTrainings = trainings;
        }
    }
}
