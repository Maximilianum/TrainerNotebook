package org.maniscalco.android.trainernotebook;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.ExerciseTypeTable;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.RepetitionTable;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.TrainingTable;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.PlanTable;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class PlanListFragment extends Fragment {

    private static final int XML_FILE_SELECT = 1;
    private RecyclerView mPlanRecyclerView;
    private PlanAdapter mPlanAdapter;
    private TextView mTextView;
    private Button mButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_plan_list, container, false);

        mPlanRecyclerView = (RecyclerView) view.findViewById(R.id.plan_recycler_view);
        mPlanRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mTextView = (TextView)view.findViewById(R.id.empty_plans_list_message);
        mButton = (Button)view.findViewById(R.id.add_new_plan_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPlan();
            }
        });

        /*if (savedInstanceState != null) {
            mSubtitleVisible = savedInstanceState.getBoolean(SAVED_SUBTITLE_VISIBLE);
        }*/

        updateUI();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    /*@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SAVED_SUBTITLE_VISIBLE, mSubtitleVisible);
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_plan_list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_plan:
                newPlan();
                return true;
            case R.id.menu_item_exercise_types:
                Intent intent = new Intent(getActivity(), ExerciseTypeListActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_item_import:
                Intent xmlIntent = new Intent(Intent.ACTION_GET_CONTENT);
                xmlIntent.setType("text/xml");
                xmlIntent.addCategory(Intent.CATEGORY_OPENABLE);
                Intent xmlchooser = Intent.createChooser(xmlIntent, getActivity().getResources().getString(R.string.xml_chooser));
                try {
                    startActivityForResult(xmlchooser, XML_FILE_SELECT);
                } catch (Exception ex) {
                    System.out.println("browse click: " + ex);
                }
                return true;
            case R.id.menu_item_about:

                Toast toast = Toast.makeText(getActivity(), R.string.credits, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == XML_FILE_SELECT) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yyyy");
            String curTag = null;
            Uri uri = data.getData();
            try {
                String filepath = null;
                if (uri != null && uri.getScheme().equals("content")) {

                    ExerciseType exeType = null;
                    int count = 0;
                    InputStream xmlis = getActivity().getContentResolver().openInputStream(uri);
                    XmlPullParserFactory xppf = XmlPullParserFactory.newInstance();
                    xppf.setNamespaceAware(true);
                    XmlPullParser xpp = xppf.newPullParser();
                    xpp.setInput(xmlis, null);
                    int e = xpp.getEventType();
                    while (e != XmlPullParser.END_DOCUMENT) {
                        switch (e) {
                            case XmlPullParser.START_TAG:
                                curTag = xpp.getName();
                                if (curTag.equals(ExerciseTypeTable.NAME)) {
                                    exeType = new ExerciseType();
                                }
                                break;
                            case XmlPullParser.END_TAG:
                                curTag = null;
                                if (xpp.getName().equals(ExerciseTypeTable.NAME)) {
                                    TrainerLab.get(getActivity()).addExerciseType(exeType);
                                    count++;
                                    exeType = null;
                                }
                                break;
                            case XmlPullParser.TEXT:
                                if (curTag != null) {
                                    if (exeType != null) {
                                        if (curTag.equals(ExerciseTypeTable.Cols.TITLE)) {
                                            exeType.setTitle(xpp.getText());
                                        } else if (curTag.equals(ExerciseTypeTable.Cols.TARGET)) {
                                            exeType.setTarget(xpp.getText());
                                        }
                                    }
                                    /*if (curTag.equals("series")) {
                                        exrc.setSeries(xpp.getText());
                                    } else if (curTag.equals("repetition")) {
                                        exrc.setRepetition(Integer.parseInt(xpp.getText()));
                                    } else if (curTag.equals("load")) {
                                        exrc.setLoad(xpp.getText());
                                    } else if (curTag.equals("training")) {
                                        exrc.setTraining(Integer.parseInt(xpp.getText()));
                                    } else if (curTag.equals("order")) {
                                        exrc.setOrder(Integer.parseInt(xpp.getText()));
                                    } else if (curTag.equals("creation_date")) {
                                        Date date = dateFormat.parse(xpp.getText());
                                        exrc.setCreationDateMillis(date.getTime());
                                    }*/
                                }
                                break;
                        }
                        e = xpp.next();
                    }
                    updateUI();
                    String imported = getResources().getQuantityString(R.plurals.imported, count, count);
                    Toast toast = Toast.makeText(getActivity(), imported, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                }
            } catch (IOException io) {
                io.printStackTrace();
            } catch (XmlPullParserException xppe) {
                xppe.printStackTrace();
            }
        }
    }

    private void updateUI() {
        TrainerLab trainerLab = TrainerLab.get(getActivity());
        List<Plan> plans = trainerLab.getPlans(null, null);

        if (plans.size() > 0) {
            mTextView.setVisibility(View.GONE);
            mButton.setVisibility(View.GONE);
        } else {
            mTextView.setVisibility(View.VISIBLE);
            mButton.setVisibility(View.VISIBLE);
        }

        if (mPlanAdapter == null) {
            mPlanAdapter = new PlanAdapter(plans);
            mPlanRecyclerView.setAdapter(mPlanAdapter);
        } else {
            mPlanAdapter.setPlans(plans);
            mPlanAdapter.notifyDataSetChanged();
        }
    }

    private void newPlan() {
        Plan plan= new Plan();
        TrainerLab.get(getActivity()).addPlan(plan);
        Intent intent = TrainingListActivity.newIntent(getActivity(), plan.getId());
        startActivity(intent);
    }

    private class PlanHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Plan mPlan;
        private TextView mStartDateTextView;
        private TextView mDaysTextView;

        public PlanHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            mStartDateTextView = (TextView) itemView.findViewById(R.id.item_list_plan_start);
            mDaysTextView = (TextView) itemView.findViewById(R.id.item_list_plan_days);
        }

        public void bindPlan(Plan plan) {
            mPlan = plan;
            mStartDateTextView.setText(String.format(DateFormat.getDateInstance().format(mPlan.getStartMillis())));
            mDaysTextView.setText(String.format(getResources().getString(R.string.plan_days), mPlan.getDays()));
        }

        @Override
        public void onClick(View v) {
            Intent intent = TrainingListActivity.newIntent(getActivity(), mPlan.getId());
            startActivity(intent);
        }
    }

    private class PlanAdapter extends RecyclerView.Adapter<PlanHolder> {

        private List<Plan> mPlans;

        public PlanAdapter(List<Plan> plans) {
            mPlans = plans;
        }

        @Override
        public PlanHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

            View view = layoutInflater.inflate(R.layout.item_list_plan, parent, false);

            return new PlanListFragment.PlanHolder(view);
        }

        @Override
        public void onBindViewHolder(PlanHolder holder, int position) {

            Plan plan = mPlans.get(position);
            holder.bindPlan(plan);
        }

        @Override
        public int getItemCount() {
            return mPlans.size();
        }

        public void setPlans(List<Plan> plans) {
            mPlans = plans;
        }
    }
}
