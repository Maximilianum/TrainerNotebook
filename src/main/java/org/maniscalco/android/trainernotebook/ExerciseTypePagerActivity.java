package org.maniscalco.android.trainernotebook;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;
import java.util.UUID;

public class ExerciseTypePagerActivity extends AppCompatActivity {

    private static final String EXTRA_EXERCISE_TYPE_UUID = "org.maniscalco.android.trainernotebook.exercise.type.uuid";

    private ViewPager mViewPager;
    private List<ExerciseType> mExerciseTypes;

    public static Intent newIntent(Context packageContext, UUID typeId) {
        Intent intent = new Intent(packageContext, ExerciseTypePagerActivity.class);
        intent.putExtra(EXTRA_EXERCISE_TYPE_UUID, typeId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_type_pager);

        UUID typeId = (UUID) getIntent().getSerializableExtra(EXTRA_EXERCISE_TYPE_UUID);

        mViewPager = findViewById(R.id.activity_exercise_type_pager_view_pager);

        mExerciseTypes = TrainerLab.get(this).getExerciseTypes(null, null);
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                ExerciseType eType = mExerciseTypes.get(position);
                return ExerciseTypeFragment.newInstance(eType.getId());
            }

            @Override
            public int getCount() {
                return mExerciseTypes.size();
            }
        });

        for (int i = 0; i < mExerciseTypes.size(); i++) {
            if (mExerciseTypes.get(i).getId().equals(typeId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }
}
