package org.maniscalco.android.trainernotebook;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ExerciseTypeListFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private ExerciseTypeAdapter mAdapter;
    private TextView mEmptyView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exercise_type_list, container, false);

        mRecyclerView = view.findViewById(R.id.exercise_type_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mEmptyView = view.findViewById(R.id.empty_exercise_types_list_message);

        /*if (savedInstanceState != null) {
            mSubtitleVisible = savedInstanceState.getBoolean(SAVED_SUBTITLE_VISIBLE);
        }*/

        updateUI();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    /*@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SAVED_SUBTITLE_VISIBLE, mSubtitleVisible);
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_exercise_type_list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_exercise_type:
                newExerciseType();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateUI() {
        TrainerLab trainerLab = TrainerLab.get(getActivity());
        List<ExerciseType> types = trainerLab.getExerciseTypes(null, null);

        if (types.size() > 0) {
            mEmptyView.setVisibility(View.GONE);
        } else {
            mEmptyView.setVisibility(View.VISIBLE);
        }

        if (mAdapter == null) {
            mAdapter = new ExerciseTypeAdapter(types);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setExerciseTypes(types);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void newExerciseType() {
        ExerciseType exerciseType= new ExerciseType();
        TrainerLab.get(getActivity()).addExerciseType(exerciseType);
        Intent intent = ExerciseTypePagerActivity.newIntent(getActivity(), exerciseType.getId());
        startActivity(intent);
    }

    private class ExerciseTypeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ExerciseType mExerciseType;
        private TextView mTitleView;
        private TextView mTargetView;

        public ExerciseTypeHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTitleView = itemView.findViewById(R.id.list_item_exercise_title_view);
            mTargetView = itemView.findViewById(R.id.list_item_exercise_target_view);
        }

        public void bindExerciseType(ExerciseType eType) {
            mExerciseType = eType;
            mTitleView.setText(eType.getTitle());
            mTargetView.setText(eType.getTarget());
        }

        @Override
        public void onClick(View v) {
            Intent intent = ExerciseTypePagerActivity.newIntent(getActivity(), mExerciseType.getId());
            startActivity(intent);
        }
    }

    private class ExerciseTypeAdapter extends RecyclerView.Adapter<ExerciseTypeHolder> {

        private List<ExerciseType> mExerciseTypes;

        public ExerciseTypeAdapter(List<ExerciseType> types) {
            mExerciseTypes = types;
        }

        @Override
        public ExerciseTypeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

            View view = layoutInflater.inflate(R.layout.item_list_exercise_type, parent, false);

            return new ExerciseTypeHolder(view);
        }

        @Override
        public void onBindViewHolder(ExerciseTypeHolder holder, int position) {

            ExerciseType exe = mExerciseTypes.get(position);
            holder.bindExerciseType(exe);
        }

        @Override
        public int getItemCount() {
            return mExerciseTypes.size();
        }

        public void setExerciseTypes(List<ExerciseType> types) {
            mExerciseTypes = types;
        }
    }
}
