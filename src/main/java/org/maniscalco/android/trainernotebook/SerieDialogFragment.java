package org.maniscalco.android.trainernotebook;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.UUID;

public class SerieDialogFragment extends DialogFragment {

    private static final String ARG_REPETITION_UUID = "org.maniscalco.android.trainernotebook.repetition.id";
    public static final String EXTRA_SERIE_TYPE = "org.maniscalco.android.trainernotebook.serie.type";
    public static final String EXTRA_SERIE = "org.maniscalco.android.trainernotebook.serie";
    public static final String EXTRA_SUPER = "org.maniscalco.android.trainernotebook.super";
    public static final String EXTRA_SERIE_REPETITION = "org.maniscalco.android.trainernotebook.serie.repetition";
    public static final String EXTRA_SERIE_MIN = "org.maniscalco.android.trainernotebook.serie.min";
    public static final String EXTRA_SERIE_STEP = "org.maniscalco.android.trainernotebook.serie.step";
    public static final String EXTRA_LOAD = "org.maniscalco.android.trainernotebook.load";
    public static final String EXTRA_LOAD_MIN = "org.maniscalco.android.trainernotebook.load.min";
    public static final String EXTRA_LOAD_STEP = "org.maniscalco.android.trainernotebook.load.step";

    private Repetition mRepetition;
    private RadioGroup mSerieTypeRadioGroup;
    private LinearLayout mSerieFixedLayout;
    private LinearLayout mSerieSuperLayout;
    private LinearLayout mSerieRepetitionLayout;
    private EditText mSerieField;
    private EditText mSuperField;
    private EditText mRepetitionField;
    private LinearLayout mSerieMaxLayout;
    private LinearLayout mSerieMinLayout;
    private LinearLayout mSerieStepLayout;
    private EditText mMaxSerieField;
    private EditText mMinSerieField;
    private EditText mStepSerieField;
    private EditText mMaxLoadField;
    private EditText mMinLoadField;
    private LinearLayout mLoadStepLayout;
    private EditText mLoadStepField;

    public static SerieDialogFragment newInstance(UUID repId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_REPETITION_UUID, repId);
        SerieDialogFragment fragment = new SerieDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        try {
            UUID repId = (UUID) getArguments().getSerializable(ARG_REPETITION_UUID);
            mRepetition = TrainerLab.get(getActivity()).getRepetition(repId);
        } catch (NullPointerException e) {
            System.out.println(e);
        }
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_serie, null);

        mSerieFixedLayout = view.findViewById(R.id.serie_fixed_layout);
        mSerieSuperLayout = view.findViewById(R.id.serie_super_layout);
        mSerieRepetitionLayout = view.findViewById(R.id.serie_repetition_layout);
        mSerieMaxLayout = view.findViewById(R.id.serie_max_layout);
        mSerieMinLayout = view.findViewById(R.id.serie_min_layout);
        mSerieStepLayout = view.findViewById(R.id.serie_step_layout);
        mLoadStepLayout = view.findViewById(R.id.serie_load_step_layout);

        mSerieTypeRadioGroup = view.findViewById(R.id.serie_type_radio);
        mSerieTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.fixed_serie:
                        mSerieFixedLayout.setVisibility(View.VISIBLE);
                        mSerieSuperLayout.setVisibility(View.VISIBLE);
                        mSerieRepetitionLayout.setVisibility(View.VISIBLE);
                        mSerieMaxLayout.setVisibility(View.GONE);
                        mSerieMinLayout.setVisibility(View.GONE);
                        mSerieStepLayout.setVisibility(View.GONE);
                        mLoadStepLayout.setVisibility(View.GONE);
                        mRepetition.getSerie().setType(SerieDefinition.Type.fixed);
                        return;
                    case R.id.pyramid_serie:
                        mSerieFixedLayout.setVisibility(View.GONE);
                        mSerieSuperLayout.setVisibility(View.GONE);
                        mSerieRepetitionLayout.setVisibility(View.GONE);
                        mSerieMaxLayout.setVisibility(View.VISIBLE);
                        mSerieMinLayout.setVisibility(View.VISIBLE);
                        mSerieStepLayout.setVisibility(View.VISIBLE);
                        mLoadStepLayout.setVisibility(View.VISIBLE);
                        mRepetition.getSerie().setType(SerieDefinition.Type.pyramid);
                        return;
                    case R.id.rhombus_serie:
                        mSerieFixedLayout.setVisibility(View.GONE);
                        mSerieSuperLayout.setVisibility(View.GONE);
                        mSerieRepetitionLayout.setVisibility(View.GONE);
                        mSerieMaxLayout.setVisibility(View.VISIBLE);
                        mSerieMinLayout.setVisibility(View.VISIBLE);
                        mSerieStepLayout.setVisibility(View.VISIBLE);
                        mLoadStepLayout.setVisibility(View.VISIBLE);
                        mRepetition.getSerie().setType(SerieDefinition.Type.rhombus);
                        return;
                }
            }
        });
        switch (mRepetition.getSerie().getType()) {
            case fixed:
                mSerieTypeRadioGroup.check(R.id.fixed_serie);
                break;
            case pyramid:
                mSerieTypeRadioGroup.check(R.id.pyramid_serie);
                break;
            case rhombus:
                mSerieTypeRadioGroup.check(R.id.rhombus_serie);
                break;
        }

        mSerieField = view.findViewById(R.id.serie_serie);
        mSerieField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals("")) {
                    if (charSequence.toString().matches("\\d+")) {
                        int val = Integer.parseInt(charSequence.toString());
                        if (val > 0) {
                            mRepetition.getSerie().setSerie(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mRepetition.getSerie().setSerie(0);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mSuperField = view.findViewById(R.id.serie_super);
        mSuperField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals("")) {
                    if (charSequence.toString().matches("\\d+")) {
                        int val = Integer.parseInt(charSequence.toString());
                        if (val > 0) {
                            mRepetition.getSerie().setSuperSerie(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mRepetition.getSerie().setSuperSerie(0);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mRepetitionField = view.findViewById(R.id.serie_repetition);
        mRepetitionField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals("")) {
                    if (charSequence.toString().matches("\\d+")) {
                        int val = Integer.parseInt(charSequence.toString());
                        if (val > 0) {
                            mRepetition.getSerie().setRepetition(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mRepetition.getSerie().setRepetition(0);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mMaxSerieField = view.findViewById(R.id.serie_max);
        mMaxSerieField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals("")) {
                    if (charSequence.toString().matches("\\d+")) {
                        int val = Integer.parseInt(charSequence.toString());
                        if (val > 0) {
                            mRepetition.getSerie().setSerie(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mRepetition.getSerie().setSerie(0);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mMinSerieField = view.findViewById(R.id.serie_min);
        mMinSerieField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals("")) {
                    if (charSequence.toString().matches("\\d+")) {
                        int val = Integer.parseInt(charSequence.toString());
                        if (val > 0) {
                            mRepetition.getSerie().setSerieMin(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mRepetition.getSerie().setSerieMin(0);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mStepSerieField = view.findViewById(R.id.serie_step);
        mStepSerieField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals("")) {
                    if (charSequence.toString().matches("\\d+")) {
                        int val = Integer.parseInt(charSequence.toString());
                        if (val > 0) {
                            mRepetition.getSerie().setSerieStep(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mRepetition.getSerie().setSerieStep(0);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mLoadStepField = view.findViewById(R.id.serie_load_step);
        mLoadStepField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")) {
                    /* regex
                     * ^ start of the string
                     * \d digit, in String have to use double \\ in order to escape the backslash
                     * + one or more
                     * () group of statement, ? at the end it means the statement is optional
                     * \. the character . the backslash is double because inside a String
                     * so the expression is a string which start with one or more digit, it can optionally have a . and can optionally have one or more digit after the .
                     */
                    if (s.toString().matches("^\\d+(\\.(\\d+)?)?")) {
                        double val = Double.parseDouble(s.toString());
                        if (val > 0.0d) {
                            mRepetition.getSerie().setLoadStep(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mRepetition.getSerie().setLoadStep(0.0d);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        switch (mRepetition.getSerie().getType()) {
            case fixed:
                mSerieField.setText(String.valueOf(mRepetition.getSerie().getSerie()));
                mSuperField.setText(String.valueOf(mRepetition.getSerie().getSuperSerie()));
                mRepetitionField.setText(String.valueOf(mRepetition.getSerie().getRepetition()));
                break;
            case pyramid:
            case rhombus:
                mMaxSerieField.setText(String.valueOf(mRepetition.getSerie().getSerie()));
                mMinSerieField.setText(String.valueOf(mRepetition.getSerie().getSerieMin()));
                mStepSerieField.setText(String.valueOf(mRepetition.getSerie().getSerieStep()));
                mLoadStepField.setText(String.valueOf(mRepetition.getSerie().getLoadStep()));
                break;
        }

        mMaxLoadField = view.findViewById(R.id.serie_load_max);
        mMaxLoadField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals("")) {
                    /* regex
                     * ^ start of the string
                     * \d digit, in String have to use double \\ in order to escape the backslash
                     * + one or more
                     * () group of statement, ? at the end it means the statement is optional
                     * \. the character . the backslash is double because inside a String
                     * so the expression is a string which start with one or more digit, it can optionally have a . and can optionally have one or more digit after the .
                     */
                    if (charSequence.toString().matches("^\\d+(\\.(\\d+)?)?")) {
                        double val = Double.parseDouble(charSequence.toString());
                        if (val > 0.0d) {
                            mRepetition.getSerie().setLoad(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mRepetition.getSerie().setLoad(0.0d);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mMinLoadField = view.findViewById(R.id.serie_load_min);
        mMinLoadField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals("")) {
                    /* regex
                     * ^ start of the string
                     * \d digit, in String have to use double \\ in order to escape the backslash
                     * + one or more
                     * () group of statement, ? at the end it means the statement is optional
                     * \. the character . the backslash is double because inside a String
                     * so the expression is a string which start with one or more digit, it can optionally have a . and can optionally have one or more digit after the .
                     */
                    if (charSequence.toString().matches("^\\d+(\\.(\\d+)?)?")) {
                        double val = Double.parseDouble(charSequence.toString());
                        if (val > 0.0d) {
                            mRepetition.getSerie().setLoadMin(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mRepetition.getSerie().setLoadMin(0.0d);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mMaxLoadField.setText(String.valueOf(mRepetition.getSerie().getLoad()));
        mMinLoadField.setText(String.valueOf(mRepetition.getSerie().getLoadMin()));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sendResult(Activity.RESULT_OK);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
        return builder.create();
    }

    private void sendResult(int resultCode) {
        if (getTargetFragment() != null) {
            Intent intent = new Intent();
            SerieDefinition serieDef = mRepetition.getSerie();
            intent.putExtra(EXTRA_SERIE_TYPE, serieDef.getTypeString());
            intent.putExtra(EXTRA_SERIE, serieDef.getSerie());
            switch (serieDef.getType()) {
                case fixed:
                    intent.putExtra(EXTRA_SUPER, serieDef.getSuperSerie());
                    intent.putExtra(EXTRA_SERIE_REPETITION, serieDef.getRepetition());
                    break;
                case pyramid:
                case rhombus:
                    intent.putExtra(EXTRA_SERIE_MIN, serieDef.getSerieMin());
                    intent.putExtra(EXTRA_SERIE_STEP, serieDef.getSerieStep());
                    intent.putExtra(EXTRA_LOAD_STEP, serieDef.getLoadStep());
                    break;
            }
            intent.putExtra(EXTRA_LOAD, serieDef.getLoad());
            intent.putExtra(EXTRA_LOAD_MIN, serieDef.getLoadMin());
            getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
        }
    }
}
