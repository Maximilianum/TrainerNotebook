package org.maniscalco.android.trainernotebook;

import java.util.Date;
import java.util.UUID;

public class Plan {
    private UUID mId;
    private long mStartMillis;
    private int mDays;

    public Plan () {
        // Generate unique identifier
        this(UUID.randomUUID());
        mStartMillis = System.currentTimeMillis();
        mDays = 90;
    }

    public Plan(UUID id) { mId = id; }

    public UUID getId() {
        return mId;
    }

    public long getStartMillis() {
        return mStartMillis;
    }

    public Date getStartDate() {
        return new Date(mStartMillis);
    }

    public void setStartMillis(long millis) {
        this.mStartMillis = millis;
    }

    public int getDays() {
        return mDays;
    }

    public void setDays(int mDays) {
        this.mDays = mDays;
    }
}
