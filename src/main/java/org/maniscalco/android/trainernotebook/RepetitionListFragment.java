package org.maniscalco.android.trainernotebook;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.maniscalco.android.trainernotebook.database.TrainerDBSchema;
import java.text.DateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class RepetitionListFragment extends Fragment {

    private static final String ARG_TRAINING_UUID = "org.maniscalco.android.trainernotebook.training.uuid";

    private Training mTraining;
    private RecyclerView mRecyclerView;
    private RepetitionAdapter mAdapter;
    private TextView mEmptyView;
    private Button mButton;

    public static RepetitionListFragment newInstance(UUID traId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_TRAINING_UUID, traId);
        RepetitionListFragment fragment = new RepetitionListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        try {
            UUID traId = (UUID) getArguments().getSerializable(ARG_TRAINING_UUID);
            mTraining = TrainerLab.get(getActivity()).getTraining(traId);
        } catch (NullPointerException e) {
            System.out.println(e);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repetition_list, container, false);

        mRecyclerView = view.findViewById(R.id.repetition_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mEmptyView = view.findViewById(R.id.empty_repetitions_list_message);
        mButton = view.findViewById(R.id.add_new_repetition_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newRepetition();
            }
        });

        /*if (savedInstanceState != null) {
            mSubtitleVisible = savedInstanceState.getBoolean(SAVED_SUBTITLE_VISIBLE);
        }*/

        updateUI();

        return view;
    }

    /*@Override
    public void onPause() {
        super.onPause();
    }*/

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    /*@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SAVED_SUBTITLE_VISIBLE, mSubtitleVisible);
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_repetition_list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_repetition:
                newRepetition();
                return true;
            case R.id.menu_item_delete_training:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                TrainerLab.get(getActivity()).removeTraining(mTraining);
                                getActivity().finish();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.confirm_delete_training).setPositiveButton(R.string.yes, dialogClickListener).setNegativeButton(R.string.no, dialogClickListener).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateUI() {
        TrainerLab trainerLab = TrainerLab.get(getActivity());
        List<Repetition> repetitions = trainerLab.getRepetitions(TrainerDBSchema.RepetitionTable.Cols.TRAINING + " = ?", new String[]{mTraining.getId().toString()});

        if (repetitions.size() > 0) {
            mEmptyView.setVisibility(View.GONE);
            mButton.setVisibility(View.GONE);
        } else {
            mEmptyView.setVisibility(View.VISIBLE);
            mButton.setVisibility(View.VISIBLE);
        }

        if (mAdapter == null) {
            mAdapter = new RepetitionAdapter(repetitions);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setRepetitions(repetitions);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void newRepetition() {
        ExerciseType exeType = null;
        List<ExerciseType> exerciseTypes = TrainerLab.get(getActivity()).getExerciseTypes(null, null);
        if (exerciseTypes.size() == 0) {
            exeType = new ExerciseType();
            exeType.setTitle(getResources().getString(R.string.new_exercise_type));
            TrainerLab.get(getActivity()).addExerciseType(exeType);
        } else {
            exeType = exerciseTypes.get(0);
        }
        Repetition rep = new Repetition();
        rep.setType(exeType.getId());
        rep.setTraining(mTraining.getId());
        TrainerLab.get(getActivity()).addRepetition(rep);
        Intent intent = RepetitionPagerActivity.newIntent(getActivity(), rep.getId());
        startActivity(intent);
    }

    private class RepetitionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Repetition mRepetition;
        private TextView mOrderView;
        private TextView mExerciseView;
        private TextView mLastTimeView;
        private TextView mSeriesView;

        public RepetitionHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mOrderView = itemView.findViewById(R.id.list_item_repetition_order_view);
            mExerciseView = itemView.findViewById(R.id.list_item_repetition_exercise_view);
            mLastTimeView = itemView.findViewById(R.id.list_item_repetition_last_time);
            mSeriesView = itemView.findViewById(R.id.list_item_repetition_series_view);
        }

        public void bindRepetition(Repetition rep) {
            mRepetition = rep;
            mOrderView.setText(String.valueOf(mRepetition.getOrder()));
            ExerciseType exeType = TrainerLab.get(getActivity()).getExerciseType(mRepetition.getType());
            mExerciseView.setText(exeType.getTitle());
            if (mRepetition.getLastTime() > 0) {
                mLastTimeView.setText(String.format(getResources().getString(R.string.exercise_last_time_label) + " %1$s  |", DateFormat.getDateInstance().format(mRepetition.getLastTime())));
            } else {
                mLastTimeView.setText(getResources().getString(R.string.never_done) + "  |");
            }
            mSeriesView.setText(mRepetition.getSerie().getSeriesString());
        }

        @Override
        public void onClick(View v) {
            Intent intent = RepetitionPagerActivity.newIntent(getActivity(), mRepetition.getId());
            startActivity(intent);
        }
    }

    private class RepetitionAdapter extends RecyclerView.Adapter<RepetitionHolder> {

        private List<Repetition> mRepetitions;

        public RepetitionAdapter(List<Repetition> repetitions) {
            mRepetitions = repetitions;
        }

        @Override
        public RepetitionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

            View view = layoutInflater.inflate(R.layout.item_list_repetition, parent, false);

            return new RepetitionHolder(view);
        }

        @Override
        public void onBindViewHolder(RepetitionHolder holder, int position) {

            Repetition rep = mRepetitions.get(position);
            holder.bindRepetition(rep);
        }

        @Override
        public int getItemCount() {
            return mRepetitions.size();
        }

        public void setRepetitions(List<Repetition> repetitions) {
            mRepetitions = repetitions;
        }
    }
}
