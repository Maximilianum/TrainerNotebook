package org.maniscalco.android.trainernotebook;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.UUID;

public class TrainingFragment extends Fragment {

    private static final String ARG_TRAINING_UUID = "org.maniscalco.android.trainernotebook.training.uuid";

    private Training mTraining;
    private EditText mNumberField;

    public static TrainingFragment newInstance(UUID traId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_TRAINING_UUID, traId);
        TrainingFragment fragment = new TrainingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            UUID traId = (UUID) getArguments().getSerializable(ARG_TRAINING_UUID);
            mTraining = TrainerLab.get(getActivity()).getTraining(traId);
        } catch (NullPointerException e) {
            System.out.println(e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mTraining.getNumber() == 0) {
            TrainerLab.get(getActivity()).removeTraining(mTraining);
        } else {
            TrainerLab.get(getActivity()).updateTraining(mTraining);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_training, container, false);

        mNumberField = view.findViewById(R.id.training_number);
        if (mTraining.getNumber() == 0) {
            mNumberField.setText("");
        } else {
            mNumberField.setText(String.valueOf(mTraining.getNumber()));
        }
        mNumberField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")) {

                    if (s.toString().matches("\\d+")) {
                        int val = Integer.parseInt(s.toString());
                        if (val > 0) {
                            mTraining.setNumber(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mTraining.setNumber(0);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                }
            }
        });

        return view;
    }
}
