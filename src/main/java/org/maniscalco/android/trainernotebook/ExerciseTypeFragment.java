package org.maniscalco.android.trainernotebook;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.UUID;

public class ExerciseTypeFragment extends Fragment {

    private static final String ARG_EXERCISE_TYPE_UUID = "org.maniscalco.android.trainernotebook.exercise.type.uuid";

    private ExerciseType mExerciseType;
    private EditText mTitleField;
    private EditText mTargetField;

    public static ExerciseTypeFragment newInstance(UUID typeId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_EXERCISE_TYPE_UUID, typeId);
        ExerciseTypeFragment fragment = new ExerciseTypeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        try {
            UUID typeId = (UUID) getArguments().getSerializable(ARG_EXERCISE_TYPE_UUID);
            mExerciseType = TrainerLab.get(getActivity()).getExerciseType(typeId);
        } catch (NullPointerException e) {
            System.out.println(e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        TrainerLab.get(getActivity()).updateExerciseType(mExerciseType);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exercise_type, container, false);

        mTitleField = view.findViewById(R.id.exercise_type_title);
        mTitleField.setText(mExerciseType.getTitle());
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mExerciseType.setTitle(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mTargetField = view.findViewById(R.id.exercise_type_target);
        mTargetField.setText(mExerciseType.getTarget());
        mTargetField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mExerciseType.setTarget(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_exercise_type_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_remove_exercise_type:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                TrainerLab.get(getActivity()).removeExerciseType(mExerciseType);
                                getActivity().finish();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.confirm_delete_exercise_type).setPositiveButton(R.string.yes, dialogClickListener).setNegativeButton(R.string.no, dialogClickListener).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
