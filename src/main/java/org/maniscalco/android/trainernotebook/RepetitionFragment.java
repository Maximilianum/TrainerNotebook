package org.maniscalco.android.trainernotebook;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.UUID;

public class RepetitionFragment extends Fragment {

    private static final String ARG_REPETITION_UUID = "org.maniscalco.android.trainernotebook.repetition.uuid";
    private static final String DIALOG_EXERCISE_SELECTION = "org.maniscalco.android.trainernotebook.dialog.exercise.selection";
    private static final String DIALOG_STOPWATCH = "org.maniscalco.android.trainernotebook.dialog.stopwatch";
    private static final String DIALOG_EDIT_SERIE = "org.maniscalco.android.trainernotebook.dialog.edit.serie";

    private static final int REQUEST_EXERCISE_TYPE = 0;
    private static final int REQUEST_EXECUTION_DONE = 1;
    private static final int REQUEST_EDIT_SERIE = 2;

    private Repetition mRepetition;
    private Button mSelectExerciseButton;
    private TextView mCarriedOutView;
    private TextView mLastTimeView;
    private EditText mOrderField;
    private TextView mSeriesView;
    private TextView mLoadView;
    private EditText mBreakField;
    private EditText mNoteField;


    public static RepetitionFragment newInstance(UUID repId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_REPETITION_UUID, repId);

        RepetitionFragment fragment = new RepetitionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        UUID repId = (UUID) getArguments().getSerializable(ARG_REPETITION_UUID);
        mRepetition = TrainerLab.get(getActivity()).getRepetition(repId);
    }

    @Override
    public void onPause() {
        super.onPause();
        TrainerLab.get(getActivity()).updateRepetition(mRepetition);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repetition, container, false);
        mSelectExerciseButton = (Button) view.findViewById(R.id.exercise_type_selection_button);
        ExerciseType exeType = TrainerLab.get(getActivity()).getExerciseType(mRepetition.getType());
        mSelectExerciseButton.setText(exeType.getTitle());
        mSelectExerciseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getFragmentManager();
                ExerciseTypeSelectionDialogFragment dialog = ExerciseTypeSelectionDialogFragment.newInstance(mRepetition.getType());
                dialog.setTargetFragment(RepetitionFragment.this, REQUEST_EXERCISE_TYPE);
                dialog.show(manager, DIALOG_EXERCISE_SELECTION);
            }
        });

        mCarriedOutView = view.findViewById(R.id.repetition_carried_out_label);
        mCarriedOutView.setText(getResources().getQuantityString(R.plurals.repetition_carried_out_label, mRepetition.getCarriedOut(), mRepetition.getCarriedOut()));

        mLastTimeView = view.findViewById(R.id.repetition_last_time);
        if (mRepetition.getLastTime() > 0) {
            mLastTimeView.setText(String.format("%1$s", DateFormat.getDateInstance().format(mRepetition.getLastTimeDate())));
        } else {
            mLastTimeView.setText(getResources().getString(R.string.never_done));
        }

        mOrderField = view.findViewById(R.id.repetition_order);
        if (mRepetition.getOrder() == 0) {
            mOrderField.setText("");
        } else {
            mOrderField.setText(String.valueOf(mRepetition.getOrder()));
        }
        mOrderField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")) {
                    if (s.toString().matches("\\d+")) {
                        int val = Integer.parseInt(s.toString());
                        if (val > 0) {
                            mRepetition.setOrder(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mRepetition.setOrder(0);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                }
            }
        });

        Button editSerieButton = view.findViewById(R.id.repetition_edit_serie_button);
        editSerieButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getFragmentManager();
                SerieDialogFragment dialog = SerieDialogFragment.newInstance(mRepetition.getId());
                dialog.setTargetFragment(RepetitionFragment.this, REQUEST_EDIT_SERIE);
                dialog.show(manager, DIALOG_EDIT_SERIE);
            }
        });

        mSeriesView = view.findViewById(R.id.repetition_series);
        mSeriesView.setText(mRepetition.getSerie().getSeriesString());

        mLoadView = view.findViewById(R.id.repetition_load);
        mLoadView.setText(String.valueOf(mRepetition.getSerie().getLoadsString()));

        mBreakField = view.findViewById(R.id.repetition_rest);
        if (mRepetition.getRest() == 0) {
            mBreakField.setText("");
        } else {
            mBreakField.setText(String.valueOf(mRepetition.getRest()));
        }
        mBreakField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")) {

                    if (s.toString().matches("\\d+")) {
                        int val = Integer.parseInt(s.toString());
                        if (val > 0) {
                            mRepetition.setRest(val);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }
                    } else {
                        mRepetition.setRest(0);
                        Toast toast = Toast.makeText(getActivity(), R.string.numeric_only, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    Toast toast = Toast.makeText(getActivity(), R.string.greater_than_zero, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                }
            }
        });

        mNoteField = view.findViewById(R.id.repetition_note);
        mNoteField.setText(mRepetition.getNote());
        mNoteField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mRepetition.setNote(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_repetition_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_execute_repetition:
                TrainerLab.get(getActivity()).updateRepetition(mRepetition);
                FragmentManager manager = getFragmentManager();
                ExecuteDialogFragment dialogFragment = ExecuteDialogFragment.newInstance(mRepetition.getId());
                dialogFragment.setTargetFragment(RepetitionFragment.this, REQUEST_EXECUTION_DONE);
                dialogFragment.show(manager, DIALOG_STOPWATCH);
                return true;

            case R.id.menu_item_remove_repetition:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                TrainerLab.get(getActivity()).removeRepetition(mRepetition);
                                getActivity().finish();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.confirm_delete_repetition).setPositiveButton(R.string.yes, dialogClickListener).setNegativeButton(R.string.no, dialogClickListener).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_EXERCISE_TYPE) {
                UUID typeId = (UUID) data.getSerializableExtra(ExerciseTypeSelectionDialogFragment.EXTRA_EXERCISE_TYPE_UUID);
                mRepetition.setType(typeId);
                ExerciseType exeType = TrainerLab.get(getActivity()).getExerciseType(mRepetition.getType());
                mSelectExerciseButton.setText(exeType.getTitle());
            } else if (requestCode == REQUEST_EXECUTION_DONE) {
                mRepetition.addCarriedOut();
                mRepetition.setLastTime(System.currentTimeMillis());
                mCarriedOutView.setText(getResources().getQuantityString(R.plurals.repetition_carried_out_label, mRepetition.getCarriedOut(), mRepetition.getCarriedOut()));
                mLastTimeView.setText(String.format("%1$s", DateFormat.getDateInstance().format(mRepetition.getLastTimeDate())));
            } else if (requestCode == REQUEST_EDIT_SERIE) {
                SerieDefinition serieDef = new SerieDefinition();
                serieDef.setType((String) data.getSerializableExtra(SerieDialogFragment.EXTRA_SERIE_TYPE));
                serieDef.setSerie((int) data.getSerializableExtra(SerieDialogFragment.EXTRA_SERIE));
                switch (serieDef.getType()) {
                    case fixed:
                        serieDef.setSuperSerie((int) data.getSerializableExtra(SerieDialogFragment.EXTRA_SUPER));
                        serieDef.setRepetition((int) data.getSerializableExtra(SerieDialogFragment.EXTRA_SERIE_REPETITION));
                        break;
                    case pyramid:
                    case rhombus:
                        serieDef.setSerieMin((int) data.getSerializableExtra(SerieDialogFragment.EXTRA_SERIE_MIN));
                        serieDef.setSerieStep((int) data.getSerializableExtra(SerieDialogFragment.EXTRA_SERIE_STEP));
                        serieDef.setLoadStep((double) data.getSerializableExtra(SerieDialogFragment.EXTRA_LOAD_STEP));
                        break;
                }
                serieDef.setLoad((double) data.getSerializableExtra(SerieDialogFragment.EXTRA_LOAD));
                serieDef.setLoadMin((double) data.getSerializableExtra(SerieDialogFragment.EXTRA_LOAD_MIN));
                mRepetition.setSerie(serieDef);
                mSeriesView.setText(serieDef.getSeriesString());
                mLoadView.setText(String.valueOf(serieDef.getLoadsString()));
                TrainerLab.get(getActivity()).updateSerie(mRepetition);
            }
        }
    }
}
