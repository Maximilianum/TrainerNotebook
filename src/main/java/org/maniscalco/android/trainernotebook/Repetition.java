package org.maniscalco.android.trainernotebook;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class Repetition {
    private UUID mId;
    private UUID mTraining;
    private UUID mType;
    private SerieDefinition mSerie;
    private int mOrder;
    private int mRest;
    private int mCarriedOut;
    private long mLastTime;
    private String mNote;

    public Repetition () {
        // Generate unique identifier
        this(UUID.randomUUID());
        mSerie = new SerieDefinition();
        mOrder = 0;
        mRest = 60;
        mCarriedOut = 0;
        mLastTime = 0;
    }

    public Repetition(UUID id) { mId = id; }

    public UUID getId() {
        return mId;
    }

    public UUID getTraining() {
        return mTraining;
    }

    public void setTraining(UUID trainingId) {
        this.mTraining = trainingId;
    }

    public int getOrder() {
        return mOrder;
    }

    public void setOrder(int mOrder) {
        this.mOrder = mOrder;
    }

    public UUID getType() {
        return mType;
    }

    public void setType(UUID typeId) {
        this.mType = typeId;
    }

    public SerieDefinition getSerie() {
        return mSerie;
    }

    public void setSerie(SerieDefinition mSerie) {
        this.mSerie = mSerie;
    }

    public int getRest() {
        return mRest;
    }

    public void setRest(int mRest) {
        this.mRest = mRest;
    }

    public int getCarriedOut() {
        return mCarriedOut;
    }

    public void setCarriedOut(int mCarriedOut) {
        this.mCarriedOut = mCarriedOut;
    }

    public void addCarriedOut() { mCarriedOut++; }

    public long getLastTime() {
        return mLastTime;
    }

    public Date getLastTimeDate() { return new Date(mLastTime); }

    public void setLastTime(long mLastTime) {
        this.mLastTime = mLastTime;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String mNote) {
        this.mNote = mNote;
    }
}
