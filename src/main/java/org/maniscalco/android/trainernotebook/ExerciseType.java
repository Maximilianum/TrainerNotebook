package org.maniscalco.android.trainernotebook;

import java.util.UUID;

public class ExerciseType {

    private UUID mId;
    private String mTitle;
    private String mTarget;

    public ExerciseType() {
        // Generate unique identifier
        this(UUID.randomUUID());
    }

    public ExerciseType(UUID id) {
        mId = id;
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTarget() {
        return mTarget;
    }

    public void setTarget(String target) {
        mTarget = target;
    }

}
