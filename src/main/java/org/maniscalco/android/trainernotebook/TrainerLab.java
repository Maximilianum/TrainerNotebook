package org.maniscalco.android.trainernotebook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.maniscalco.android.trainernotebook.database.TrainerBaseHelper;
import org.maniscalco.android.trainernotebook.database.TrainerCursorWrapper;

import org.maniscalco.android.trainernotebook.database.TrainerDBSchema;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.ExerciseTypeTable;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.RepetitionTable;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.TrainingTable;
import org.maniscalco.android.trainernotebook.database.TrainerDBSchema.PlanTable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class TrainerLab {

    private static TrainerLab sTrainerLab;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static TrainerLab get(Context context) {
        if (sTrainerLab == null) {
            sTrainerLab = new TrainerLab(context);
        }

        return sTrainerLab;
    }

    private TrainerLab(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new TrainerBaseHelper(mContext).getWritableDatabase();
    }

    private static ContentValues getContentValues(ExerciseType exerciseType) {
        ContentValues values = new ContentValues();
        values.put(ExerciseTypeTable.Cols.UUID, exerciseType.getId().toString());
        values.put(ExerciseTypeTable.Cols.TITLE, exerciseType.getTitle());
        values.put(ExerciseTypeTable.Cols.TARGET, exerciseType.getTarget());
        return values;
    }

    private static ContentValues getContentValues(Repetition repetition) {
        ContentValues values = new ContentValues();
        values.put(RepetitionTable.Cols.UUID, repetition.getId().toString());
        values.put(RepetitionTable.Cols.TRAINING, repetition.getTraining().toString());
        values.put(RepetitionTable.Cols.TYPE, repetition.getType().toString());
        values.put(RepetitionTable.Cols.SERIE_TYPE, repetition.getSerie().getTypeString());
        values.put(RepetitionTable.Cols.SERIE, repetition.getSerie().getSerie());
        switch (repetition.getSerie().getType()) {
            case fixed:
                values.put(RepetitionTable.Cols.SUPER, repetition.getSerie().getSuperSerie());
                values.put(RepetitionTable.Cols.REPETITION, repetition.getSerie().getRepetition());
                break;
            case pyramid:
            case rhombus:
                values.put(RepetitionTable.Cols.SERIE_MIN, repetition.getSerie().getSerieMin());
                values.put(RepetitionTable.Cols.SERIE_STEP, repetition.getSerie().getSerieStep());
                values.put(RepetitionTable.Cols.LOAD_STEP, repetition.getSerie().getLoadStep());
                break;
        }
        values.put(RepetitionTable.Cols.LOAD, repetition.getSerie().getLoad());
        values.put(RepetitionTable.Cols.LOAD_MIN, repetition.getSerie().getLoadMin());
        values.put(RepetitionTable.Cols.ORDER, repetition.getOrder());
        values.put(RepetitionTable.Cols.REST, repetition.getRest());
        values.put(RepetitionTable.Cols.CARRIED_OUT, repetition.getCarriedOut());
        values.put(RepetitionTable.Cols.LAST_TIME, repetition.getLastTime());
        values.put(RepetitionTable.Cols.NOTE, repetition.getNote());
        return values;
    }

    private static ContentValues getContentValues(SerieDefinition serie) {
        ContentValues values = new ContentValues();
        values.put(RepetitionTable.Cols.SERIE_TYPE, serie.getTypeString());
        values.put(RepetitionTable.Cols.SERIE, serie.getSerie());
        switch (serie.getType()) {
            case fixed:
                values.put(RepetitionTable.Cols.SUPER, serie.getSuperSerie());
                values.put(RepetitionTable.Cols.REPETITION, serie.getRepetition());
                break;
            case pyramid:
            case rhombus:
                values.put(RepetitionTable.Cols.SERIE_MIN, serie.getSerieMin());
                values.put(RepetitionTable.Cols.SERIE_STEP, serie.getSerieStep());
                values.put(RepetitionTable.Cols.LOAD_STEP, serie.getLoadStep());
                break;
        }
        values.put(RepetitionTable.Cols.LOAD, serie.getLoad());
        values.put(RepetitionTable.Cols.LOAD_MIN, serie.getLoadMin());
        return values;
    }

    private static ContentValues getContentValues(Training training) {
        ContentValues values = new ContentValues();
        values.put(TrainingTable.Cols.UUID, training.getId().toString());
        values.put(TrainingTable.Cols.PLAN, training.getPlan().toString());
        values.put(TrainingTable.Cols.NUMBER, training.getNumber());
        return values;
    }

    private static ContentValues getContentValues(Plan plan) {
        ContentValues values = new ContentValues();
        values.put(TrainerDBSchema.PlanTable.Cols.UUID, plan.getId().toString());
        values.put(TrainerDBSchema.PlanTable.Cols.START, plan.getStartMillis());
        values.put(TrainerDBSchema.PlanTable.Cols.DAYS, plan.getDays());
        return values;
    }

    public List<ExerciseType> getExerciseTypes(String whereClause, String[] whereArgs) {

        List<ExerciseType> exerciseTypes = new ArrayList<>();

        TrainerCursorWrapper cursor = queryExerciseTypes(whereClause, whereArgs);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                exerciseTypes.add(cursor.getExerciseType());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return exerciseTypes;
    }

    public ExerciseType getExerciseType(UUID id) {

        TrainerCursorWrapper cursor = queryExerciseTypes(
                ExerciseTypeTable.Cols.UUID + " = ?", new String[] {id.toString()}
        );

        try {
            if (cursor.getCount() == 0) {
                return null;
            }

            cursor.moveToFirst();
            return cursor.getExerciseType();
        } finally {
            cursor.close();
        }
    }

    public void addExerciseType(ExerciseType et) {
        ContentValues values = getContentValues(et);
        mDatabase.insert(ExerciseTypeTable.NAME, null, values);
    }

    public void updateExerciseType(ExerciseType et) {
        String uuidString = et.getId().toString();
        ContentValues values = getContentValues(et);

        mDatabase.update(ExerciseTypeTable.NAME, values, ExerciseTypeTable.Cols.UUID + " = ?", new String[] { uuidString });
    }

    public void removeExerciseType(ExerciseType et) {
        int deletedRows = mDatabase.delete(ExerciseTypeTable.NAME, ExerciseTypeTable.Cols.UUID + "= ?", new String[] { et.getId().toString() });
    }

    public List<Repetition> getRepetitions(String whereClause, String[] whereArgs) {

        List<Repetition> repetitions = new ArrayList<>();

        TrainerCursorWrapper cursor = queryRepetitions(whereClause, whereArgs);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                repetitions.add(cursor.getRepetition());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return repetitions;
    }

    public Repetition getRepetition(UUID id) {

        TrainerCursorWrapper cursor = queryRepetitions(
                RepetitionTable.Cols.UUID + " = ?", new String[] {id.toString()}
        );

        try {
            if (cursor.getCount() == 0) {
                return null;
            }

            cursor.moveToFirst();
            return cursor.getRepetition();
        } finally {
            cursor.close();
        }
    }

    public void addRepetition(Repetition rep) {
        ContentValues values = getContentValues(rep);
        mDatabase.insert(RepetitionTable.NAME, null, values);
    }

    public void updateRepetition(Repetition rep) {
        String uuidString = rep.getId().toString();
        ContentValues values = getContentValues(rep);

        mDatabase.update(RepetitionTable.NAME, values, RepetitionTable.Cols.UUID + " = ?", new String[] { uuidString });
    }

    public void updateSerie(Repetition rep) {
        String uuidString = rep.getId().toString();
        ContentValues values = getContentValues(rep.getSerie());

        mDatabase.update(RepetitionTable.NAME, values, RepetitionTable.Cols.UUID + " = ?", new String[] { uuidString });
    }

    public void removeRepetition(Repetition rep) {
        int deletedRows = mDatabase.delete(RepetitionTable.NAME, RepetitionTable.Cols.UUID + "= ?", new String[] { rep.getId().toString() });
    }

    public List<Training> getTrainings(String whereClause, String[] whereArgs) {

        List<Training> trainings = new ArrayList<>();

        TrainerCursorWrapper cursor = queryTrainings(whereClause, whereArgs);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                trainings.add(cursor.getTraining());
                cursor.moveToNext();

            }
        } finally {
            cursor.close();
        }

        return trainings;
    }

    public Training getTraining(UUID id) {

        TrainerCursorWrapper cursor;

        if (id != null) {
            cursor = queryTrainings(
                    TrainingTable.Cols.UUID + " = ?", new String[]{id.toString()}
            );
        } else {
            cursor = queryTrainings(null, null);
        }

        try {
            if (cursor.getCount() == 0) {
                return null;
            }

            cursor.moveToFirst();
            return cursor.getTraining();
        } finally {
            cursor.close();
        }
    }

    public Training getTraining(int number) {

        TrainerCursorWrapper cursor;

        if (number != 0) {
            cursor = queryTrainings(
                    TrainingTable.Cols.NUMBER + " = ?", new String[] {String.valueOf(number)}
            );
        } else {
            cursor = queryTrainings(null, null);
        }

        try {
            if (cursor.getCount() == 0) {
                return null;
            }

            cursor.moveToFirst();
            return cursor.getTraining();
        } finally {
            cursor.close();
        }
    }

    public void addTraining(Training training) {
        ContentValues values = getContentValues(training);
        mDatabase.insert(TrainingTable.NAME, null, values);
    }

    public void updateTraining(Training tra) {
        String uuidString = tra.getId().toString();
        ContentValues values = getContentValues(tra);
        mDatabase.update(TrainingTable.NAME, values, TrainingTable.Cols.UUID + " = ?", new String[] { uuidString });
    }

    public void removeTraining(Training tra) {

        List<Repetition> reps = getRepetitions(RepetitionTable.Cols.TRAINING + " = ?", new String[] { tra.getId().toString()});
        Iterator<Repetition> iter = reps.iterator();
        while (iter.hasNext()) {
            removeRepetition(iter.next());
        }
        int deletedRows = mDatabase.delete(TrainingTable.NAME, TrainingTable.Cols.UUID + "= ?", new String[] { tra.getId().toString() });
    }

    public List<Plan> getPlans(String whereClause, String[] whereArgs) {

        List<Plan> plans = new ArrayList<>();

        TrainerCursorWrapper cursor = queryPlans(whereClause, whereArgs);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                plans.add(cursor.getPlan());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return plans;
    }

    public Plan getPlan(UUID id) {

        TrainerCursorWrapper cursor;

        if (id != null) {
            cursor = queryPlans(
                    PlanTable.Cols.UUID + " = ?", new String[]{id.toString()}
            );
        } else {
            cursor = queryPlans(null, null);
        }

        try {
            if (cursor.getCount() == 0) {
                return null;
            }

            cursor.moveToFirst();
            return cursor.getPlan();
        } finally {
            cursor.close();
        }
    }

    public void addPlan(Plan plan) {
        ContentValues values = getContentValues(plan);
        mDatabase.insert(PlanTable.NAME, null, values);
    }

    public void updatePlan(Plan plan) {
        String uuidString = plan.getId().toString();
        ContentValues values = getContentValues(plan);
        mDatabase.update(PlanTable.NAME, values, PlanTable.Cols.UUID + " = ?", new String[] { uuidString });
    }

    public void removePlan(Plan plan) {

        List<Training> trainings = getTrainings(TrainingTable.Cols.PLAN + " = ?", new String[] { plan.getId().toString()});
        Iterator<Training> iter = trainings.iterator();
        while (iter.hasNext()) {
            removeTraining(iter.next());
        }
        int deletedRows = mDatabase.delete(PlanTable.NAME, PlanTable.Cols.UUID + "= ?", new String[] { plan.getId().toString() });
    }

    private TrainerCursorWrapper queryExerciseTypes(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(ExerciseTypeTable.NAME,
                null, //Columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                String.format("%1$s ASC, %2$s ASC", ExerciseTypeTable.Cols.TARGET, ExerciseTypeTable.Cols.TITLE) // orderBy
        );

        return new TrainerCursorWrapper(cursor);
    }

    private TrainerCursorWrapper queryRepetitions(String whereClause, String[] whereArgs) {
        /*String sql = "select " +
                RepetitionTable.NAME + "." + RepetitionTable.Cols.UUID + ", " +
                RepetitionTable.NAME + "." + RepetitionTable.Cols.TRAINING + ", " +
                ExerciseTable.NAME + "." + ExerciseTable.Cols.UUID + ", " +
                ExerciseTable.NAME + "." + ExerciseTable.Cols.TITLE + ", " +
                ExerciseTable.NAME + "." + ExerciseTable.Cols.TARGET + ", " +
                RepetitionTable.NAME + "." + RepetitionTable.Cols.ORDER + ", " +
                RepetitionTable.NAME + "." + RepetitionTable.Cols.SERIES + ", " +
                RepetitionTable.NAME + "." + RepetitionTable.Cols.REPETITION + ", " +
                RepetitionTable.NAME + "." + RepetitionTable.Cols.LOAD + ", " +
                RepetitionTable.NAME + "." + RepetitionTable.Cols.REST + ", " +
                RepetitionTable.NAME + "." + RepetitionTable.Cols.EXECUTION + ", " +
                RepetitionTable.NAME + "." + RepetitionTable.Cols.CARRIED_OUT + ", " +
                RepetitionTable.NAME + "." + RepetitionTable.Cols.LAST_TIME + " from " + RepetitionTable.NAME +
                " inner join " + ExerciseTable.NAME + " on " +
                RepetitionTable.NAME + "." + RepetitionTable.Cols.EXERCISE + " = " + ExerciseTable.NAME + "." + ExerciseTable.Cols.UUID +
                " where " + whereClause + " order by " + RepetitionTable.NAME + "." + RepetitionTable.Cols.ORDER + " asc;";
        Cursor cursor = mDatabase.rawQuery(sql, whereArgs);*/
        Cursor cursor = mDatabase.query(RepetitionTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                String.format("%1$s ASC", RepetitionTable.Cols.ORDER)
        );

        return new TrainerCursorWrapper(cursor);
    }

    private TrainerCursorWrapper queryTrainings(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(TrainingTable.NAME,
                null, //Columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                String.format("%1$s ASC", TrainingTable.Cols.NUMBER) // orderBy
        );

        return new TrainerCursorWrapper(cursor);
    }

    private TrainerCursorWrapper queryPlans(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(PlanTable.NAME,
                null, //Columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null /*String.format("%1$s ASC", PlanTable.Cols.START)*/ // orderBy
        );

        return new TrainerCursorWrapper(cursor);
    }
}
